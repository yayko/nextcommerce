import { getProductsBySearchQuery } from "@/api/productAPI";
import { ProductList } from "@/components/organisms/ProductList";

type ProductSearchPageProps = {
	searchParams: {
		query: string;
	};
};

const ProductSearchPage = async ({ searchParams }: ProductSearchPageProps) => {
	const { products } = await getProductsBySearchQuery(searchParams);
	return <ProductList products={products} />;
};

export default ProductSearchPage;
