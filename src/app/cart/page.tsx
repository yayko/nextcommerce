import { type Route } from "next";
import { cookies } from "next/headers";
import { formatPrice } from "@/utils/productUtils";
import { ProductListItemImage } from "@/components/atoms/product/ProductListItemImage";
import { UpdateCartItemQuantityInput } from "@/components/molecules/cart/UpdateCartItemQuantityInput";
import { CartItemRemoveButton } from "@/components/atoms/cart/CartItemRemoveButton";
import { executeGraphQl } from "@/api/baseGraphQLRequest";
import { CartGetCartItemsListDocument } from "@/gql/graphql";

export async function generateMetadata() {
	return {
		title: "Cart - Nextshop",
		description: "Your Nextshop Cart",
	};
}
const CartPage = async () => {
	const cartId = cookies().get("cartId")?.value || "";
	const { cart } = await executeGraphQl({
		query: CartGetCartItemsListDocument,
		variables: {
			cartId,
		},
	});

	if (!cart || !cart.cartItems || cart.cartItems.length === 0) {
		return (
			<div>
				<h1 className="mb-8 text-3xl font-bold">Shopping Cart</h1>
				<div className="p-md-3 rounded-md border-[1px] border-slate-200 bg-slate-50 p-8 text-center">
					Cart is empty
				</div>
			</div>
		);
	}

	return (
		<div>
			<h1 className="mb-8 text-3xl font-bold">Shopping Cart</h1>
			<div className="flex w-full">
				<div className="me-20 grow">
					<div className="w-100 me-20 w-full text-sm">
						<ul className="bg-white">
							{cart.cartItems.map(({ id, product, quantity }) => (
								<li key={`cart-item-${id}`} className="mb-8 flex">
									<div className={"h-[120px] w-[120px]"}>
										<ProductListItemImage
											src={product.image}
											url={`/product/${product.slug}` as Route}
										/>
									</div>

									<div className="ms-4 flex grow flex-col justify-between">
										<div className="flex">
											<div className="me-4 grow">
												<div className={"text-xl font-bold"}>{product.name}</div>
												<div className={"mb-4 font-semibold text-red-300"}>
													{product.category.name}
												</div>
											</div>
											<div className={"text-3xl font-semibold text-red-500"}>
												{formatPrice(product.price)}
											</div>
										</div>

										<div className="flex justify-between">
											<UpdateCartItemQuantityInput cartItem={{ id, quantity }} />
											<CartItemRemoveButton cartItemId={id} />
										</div>
									</div>
								</li>
							))}
						</ul>
					</div>
				</div>
				<div className="w-[350px] flex-none ">
					<div className="Ū sticky top-20 rounded-lg border-2 border-red-400 p-4 shadow-2xl lg:p-8">
						<h1 className="mb-4 text-3xl font-bold">Summary</h1>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CartPage;
