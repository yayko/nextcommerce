import "./globals.css";
import type { Metadata, Route } from "next";
import { Poppins } from "next/font/google";
import clsx from "clsx";
import { Header } from "@/components/molecules/Header";
import { getCategories } from "@/api/categoryAPI";
import { type HeaderLinkProps } from "@/types";
import { Wave } from "@/components/atoms/Wave";
import { getCollections } from "@/api/collectionAPI";
import { CollectionsList } from "@/components/atoms/CollectionsList";

const oswaldFont = Poppins({ subsets: ["latin-ext"], weight: ["400", "600"] });

export const metadata: Metadata = {
	title: "Home - Nextshop",
	description: "Nextshop - demo e-commerce website",
};

export default async function RootLayout({ children }: { children: React.ReactNode }) {
	const collections = await getCollections();
	const categories = await getCategories();

	const categoryLinks = categories.map(
		(category): HeaderLinkProps => ({
			title: category.name,
			href: `/categories/${category.slug}/1` as Route,
			activeIfStartsWith: `/categories/${category.slug}`,
		}),
	);

	return (
		<html lang="en">
			<body className={clsx(oswaldFont.className, "flex min-h-screen flex-col -tracking-tight")}>
				<Header
					links={[
						{
							title: "Home",
							href: "/",
						},
						{
							title: "All",
							href: "/products",
						},
						...categoryLinks,
					]}
					linkClassName="hover:text-white border-b-2 border-b-transparent hover:border-b-red-300 hover:text-red-300"
					activeLinkClassName="border-b-2 text-red-500 hover:text-red-300 hover:border-b-red-300 border-b-red-500 font-bold"
				/>
				<main className="flex-grow">
					<CollectionsList collections={collections} />
					<div className="mx-auto w-full max-w-7xl px-4 py-8">{children}</div>
				</main>
				<Wave />
				<footer className="bg-gray-50 bg-neutral-800 px-4 py-8 text-center text-white">
					&copy; yayko
				</footer>
			</body>
		</html>
	);
}
