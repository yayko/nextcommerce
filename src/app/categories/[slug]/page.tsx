import { type Metadata } from "next";
import { getCategory } from "@/api/categoryAPI";
import { CategoryProductListTemplate } from "@/components/templates/CategoryProductListTemplate";
import type { GetBySlugParams } from "@/types";

export const generateMetadata = async ({
	params,
	searchParams,
}: {
	params: GetBySlugParams;
	searchParams: Record<string, string>;
}): Promise<Metadata> => {
	const { category } = await getCategory({ params, searchParams });

	return {
		title: category.name,
		openGraph: {
			title: category.name,
			url: `https://nextshop.vercel.app/categories/${params.slug}`,
			type: "article",
		},
	};
};
const CategoryPage = async (props: {
	params: GetBySlugParams;
	searchParams: Record<string, string>;
}) => <CategoryProductListTemplate {...props} />;
export default CategoryPage;
