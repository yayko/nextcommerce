import { type PageParams } from "@/types";
import { ProductListTemplate } from "@/components/templates/ProductListTemplate";

const ProductsListPage = async (props: PageParams) => <ProductListTemplate {...props} />;
export default ProductsListPage;
