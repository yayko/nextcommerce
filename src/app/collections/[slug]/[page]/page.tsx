import { type Metadata } from "next";
import { getCollection } from "@/api/collectionAPI";
import { CollectionProductListTemplate } from "@/components/templates/CollectionProductListTemplate";
import { type PageParamsWithSlug } from "@/types";

export const generateMetadata = async ({
	params,
	searchParams,
}: PageParamsWithSlug): Promise<Metadata> => {
	const {
		collection: { name },
	} = await getCollection({ params, searchParams });
	return {
		title: name,
		openGraph: {
			title: name,
			url: `https://nextshop.vercel.app/collections/${params.slug}`,
			type: "article",
		},
	};
};
const CollectionPagePaginated = async (props: PageParamsWithSlug) => (
	<CollectionProductListTemplate {...props} />
);
export default CollectionPagePaginated;
