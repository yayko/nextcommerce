import { type Route, type Metadata } from "next";
import { Suspense } from "react";
import { getProduct } from "@/api/productAPI";
import { ProductDetailsImage } from "@/components/atoms/product/ProductDetailsImage";
import { SuggestedProductsList } from "@/components/organisms/SuggestedProductsList";
import { ProductDetailsDescription } from "@/components/molecules/ProductDetailsDescription";
import { type ProductItemFragment } from "@/gql/graphql";
import { ReviewsList } from "@/components/organisms/ReviewsList";

type ProductDetailPageProps = { params: { slug: ProductItemFragment["slug"] } };

export const generateMetadata = async ({ params }: ProductDetailPageProps): Promise<Metadata> => {
	const { image, name, description } = await getProduct(params.slug);
	return {
		title: name,
		description,
		openGraph: {
			title: name,
			description,
			images: [{ url: image, alt: name }],
			type: "article",
		},
	};
};

export default async function ProductDetailPage({ params }: ProductDetailPageProps) {
	const { id, slug, image, name, price, description } = await getProduct(params.slug);
	return (
		<>
			<section className="mb-10 flex flex-row flex-wrap">
				<ProductDetailsImage src={image} alt={name} url={`/product/${slug}` as Route} />
				<ProductDetailsDescription id={id} name={name} price={price} description={description} />
			</section>
			<Suspense fallback={<div>Loading...</div>}>
				<SuggestedProductsList title="Related products" />
			</Suspense>
			<Suspense>
				<ReviewsList productId={id} />
			</Suspense>
		</>
	);
}
