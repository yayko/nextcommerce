import { Suspense } from "react";
import { SuggestedProductsList } from "@/components/organisms/SuggestedProductsList";

export default async function Home() {
	return (
		<section>
			<Suspense fallback={<div>Loading...</div>}>
				<SuggestedProductsList />
			</Suspense>
		</section>
	);
}
