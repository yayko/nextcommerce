import { type MaybePaginationParams, type PaginationParams } from "@/types";

export const defaultPageSize = 6;

export function getPaginationParams({ take, page }: MaybePaginationParams): PaginationParams {
	const _take = take || defaultPageSize;
	return {
		skip: +(page ? _take * page - _take : 0),
		take: +_take,
	};
}
