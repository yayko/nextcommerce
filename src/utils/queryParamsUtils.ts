export const getUpdatedQueryString = (
	currentQueryString: string,
	keyToUpdate: string,
	newValue: string,
) => {
	if (!currentQueryString) {
		return `${keyToUpdate}=${newValue}`;
	}

	const splitParams = currentQueryString.split("&");

	if (splitParams.some((param) => param.startsWith(`${keyToUpdate}=`)) === false) {
		return `${currentQueryString}&${keyToUpdate}=${newValue}`;
	}

	splitParams.forEach((param) => {
		const [key, value] = param.split("=");
		if (key === keyToUpdate) {
			currentQueryString = currentQueryString.replace(`${key}=${value}`, `${key}=${newValue}`);
		}
	});
	return currentQueryString;
};
