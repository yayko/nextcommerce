import type { LinkProps } from "next/link";
import { type Route } from "next";
import { type ProductListItemFragment } from "@/gql/graphql";

export type Collection = {
	id: string;
	name: string;
	slug: string;
};

export type Category = {
	id: string;
	name: string;
	slug: string;
};

export type Product = {
	id: string;
	name: string;
	slug: string;
	price: number;
	description: string;
	category: string;
	rating?: { rate: number; count: number };
	image: string;
};

export type ProductListItemDescriptionProps = {
	slug: string;
	title: string;
	price: number;
	avgRating: number;
	category?: string;
};

export type ProductImageProps = {
	src: string;
	alt?: string;
	url: Route;
	width?: number;
	height?: number;
};

export type ProductListItemProps = {
	product: ProductListItemFragment;
	categoryName?: string;
};

export type ProductListProps = {
	products: ProductListItemFragment[];
};
export type ActiveLinkProps<T extends string = string> = Omit<LinkProps<T>, "href"> & {
	activeClassName?: string;
	href: Route<T>;
	activeIfStartsWith?: string;
};

export type ActiveFilterLinkProps<T extends string = string> = ActiveLinkProps<T> & {
	isActive: boolean;
};

export type HeaderLinkProps = {
	title: string;
	href: ActiveLinkProps["href"];
	activeIfStartsWith?: string;
};
export type HeaderProps = {
	links: HeaderLinkProps[];
	linkClassName?: string;
	activeLinkClassName?: string;
};

export type MaybePaginationParams = {
	take?: number;
	page?: number;
};
export type PaginationParams = {
	take: number;
	skip: number;
};
export type GetBySlugParams = MaybePaginationParams & { slug: string };

export type PageParams = {
	params: MaybePaginationParams;
	searchParams: Record<string, string>;
};

export type PageParamsWithSlug = {
	params: GetBySlugParams;
	searchParams: Record<string, string>;
};
/**
 * 	Cart
 */

export type IncrementQuantityButtonProps = {
	onIncrement: () => Promise<void>;
	disabled: boolean;
};

export type DecrementQuantityButtonProps = {
	onDecrement: () => Promise<void>;
	disabled: boolean;
};

/**
 *  GraphQL
 */
export type GraphQLResponse<T> =
	| { data?: undefined; errors: { message: string }[] }
	| { data: T; errors?: undefined };
