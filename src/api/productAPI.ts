import { notFound } from "next/navigation";
import { executeGraphQl } from "./baseGraphQLRequest";
import {
	ProductGetBySlugDocument,
	ProductGetListDocument,
	ProductSearchQueryDocument,
	type ProductItemFragment,
} from "@/gql/graphql";
import { getPaginationParams } from "@/utils/paginationUtils";
import { type PageParams } from "@/types";

export const getProductsBySearchQuery = async (props: { query: string }) => {
	const data = await executeGraphQl({
		query: ProductSearchQueryDocument,
		variables: {
			search: props.query || "",
		},
	});

	return data;
};
export const getProducts = async ({ params, searchParams }: PageParams) =>
	executeGraphQl({
		query: ProductGetListDocument,
		variables: {
			where: {},
			...getPaginationParams({ ...params, page: params?.page || 1 }),
			sort: searchParams?.sort,
		},
	});

export const getProduct = async (slug: string): Promise<ProductItemFragment> => {
	const { productBySlug: product } = await executeGraphQl({
		query: ProductGetBySlugDocument,
		variables: { slug },
	});

	if (!product) {
		throw notFound();
	}

	return product;
};
