import { executeGraphQl } from "./baseGraphQLRequest";
import type { Category, PageParamsWithSlug } from "@/types";
import { CategoryGetListDocument, CategoryGetOneBySlugDocument } from "@/gql/graphql";
import { getPaginationParams } from "@/utils/paginationUtils";

export const getCategories = async (): Promise<Category[]> => {
	const { categories } = await executeGraphQl({ query: CategoryGetListDocument });
	return categories;
};

export const getCategory = async ({
	params: { slug, page, take },
	searchParams,
}: PageParamsWithSlug) =>
	executeGraphQl({
		query: CategoryGetOneBySlugDocument,
		variables: {
			slug,
			...getPaginationParams({ take, page: page || 1 }),
			sort: searchParams?.sort,
		},
	});
