import { executeGraphQl } from "@/api/baseGraphQLRequest";
import { GetCartTotalsDocument } from "@/gql/graphql";

export const getCartTotals = async (cartId: string) => {
	return executeGraphQl({
		query: GetCartTotalsDocument,
		variables: {
			id: cartId,
		},
		next: {
			tags: ["cartTotals"],
		},
	});
};
