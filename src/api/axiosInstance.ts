import Axios from "axios";

export const axios = Axios.create({
	baseURL: process.env.GRAPHQL_URL,
	headers: {
		common: {
			"Content-Type": "application/json",
		},
	},
	withCredentials: true,
});
