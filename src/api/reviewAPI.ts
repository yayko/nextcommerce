import { executeGraphQl } from "@/api/baseGraphQLRequest";
import { ReviewsGetListForProductDocument } from "@/gql/graphql";

export const getReviews = async (productId: string) => {
	return executeGraphQl({
		query: ReviewsGetListForProductDocument,
		variables: {
			productId,
		},
		next: {
			tags: ["reviews"],
		},
	});
};
