import { executeGraphQl } from "./baseGraphQLRequest";
import type { Collection, PageParamsWithSlug } from "@/types";
import { CollectionGetListDocument, CollectionGetOneBySlugDocument } from "@/gql/graphql";
import { getPaginationParams } from "@/utils/paginationUtils";

export const getCollections = async (): Promise<Collection[]> => {
	const { collections } = await executeGraphQl({ query: CollectionGetListDocument });
	return collections;
};

export const getCollection = async ({
	params: { slug, page, take },
	searchParams,
}: PageParamsWithSlug) =>
	executeGraphQl({
		query: CollectionGetOneBySlugDocument,
		variables: {
			slug,
			...getPaginationParams({ take, page: page || 1 }),
			sort: searchParams?.sort,
		},
	});
