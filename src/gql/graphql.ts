/* eslint-disable */
import type { DocumentTypeDecoration } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /** A date-time string at UTC, such as 2019-12-03T09:54:33Z, compliant with the date-time format. */
  DateTime: { input: unknown; output: unknown; }
};

export type Cart = {
  cartItems: Array<CartItem>;
  id: Scalars['ID']['output'];
};

export type CartItem = {
  cart: Cart;
  cartId: Scalars['String']['output'];
  createdAt: Scalars['DateTime']['output'];
  id: Scalars['ID']['output'];
  product: Product;
  productId: Scalars['String']['output'];
  productVariantId: Scalars['String']['output'];
  quantity: Scalars['Int']['output'];
};

export type CartItemAddDto = {
  cartId: Scalars['String']['input'];
  productId: Scalars['String']['input'];
};

export type CartItemRemoveDto = {
  /** The ID of the CartItem entity */
  id: Scalars['String']['input'];
};

export type CartItemUpdatedDto = {
  /** The ID of the CartItem entity */
  id: Scalars['String']['input'];
  quantity: Scalars['Int']['input'];
};

export type CartTotals = {
  productsCount: Scalars['Int']['output'];
  productsTotalPrice: Scalars['Int']['output'];
};

export type Category = {
  id: Scalars['String']['output'];
  name: Scalars['String']['output'];
  products: Array<Product>;
  slug: Scalars['String']['output'];
};


export type CategoryProductsArgs = {
  skip?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ProductsWhereInput>;
};

export type Collection = {
  id: Scalars['String']['output'];
  name: Scalars['String']['output'];
  products: Array<Product>;
  slug: Scalars['String']['output'];
};


export type CollectionProductsArgs = {
  skip?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ProductsWhereInput>;
};

export type Mutation = {
  cartCreate?: Maybe<Cart>;
  cartItemAdd: CartItem;
  cartItemRemove: CartItem;
  cartItemUpdateQuantity: CartItem;
  reviewCreate: Review;
};


export type MutationCartItemAddArgs = {
  data: CartItemAddDto;
};


export type MutationCartItemRemoveArgs = {
  data: CartItemRemoveDto;
};


export type MutationCartItemUpdateQuantityArgs = {
  data: CartItemUpdatedDto;
};


export type MutationReviewCreateArgs = {
  data: ReviewAddDto;
};

export type Product = {
  avgRating: Scalars['Float']['output'];
  category: Category;
  category_id: Scalars['String']['output'];
  collections: Array<Collection>;
  description: Scalars['String']['output'];
  id: Scalars['String']['output'];
  image: Scalars['String']['output'];
  name: Scalars['String']['output'];
  price: Scalars['Int']['output'];
  slug: Scalars['String']['output'];
};

export type ProductTotal = {
  total: Scalars['Int']['output'];
};

export type ProductsWhereInput = {
  category_name_contains?: InputMaybe<Scalars['String']['input']>;
  category_slug?: InputMaybe<Scalars['String']['input']>;
  collection_slug?: InputMaybe<Scalars['String']['input']>;
  name_contains?: InputMaybe<Scalars['String']['input']>;
  price_gte?: InputMaybe<Scalars['Int']['input']>;
  price_lte?: InputMaybe<Scalars['Int']['input']>;
};

export type Query = {
  cart?: Maybe<Cart>;
  cartTotals: CartTotals;
  categories: Array<Category>;
  category: Category;
  collection: Collection;
  collections: Array<Collection>;
  product: Product;
  productBySlug: Product;
  products: Array<Product>;
  productsTotal: ProductTotal;
  reviews: Array<Review>;
};


export type QueryCartArgs = {
  id: Scalars['String']['input'];
};


export type QueryCartTotalsArgs = {
  id: Scalars['String']['input'];
};


export type QueryCategoriesArgs = {
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
};


export type QueryCategoryArgs = {
  slug: Scalars['String']['input'];
};


export type QueryCollectionArgs = {
  slug: Scalars['String']['input'];
};


export type QueryCollectionsArgs = {
  skip?: InputMaybe<Scalars['Int']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
};


export type QueryProductArgs = {
  id: Scalars['String']['input'];
};


export type QueryProductBySlugArgs = {
  slug: Scalars['String']['input'];
};


export type QueryProductsArgs = {
  skip?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
  take?: InputMaybe<Scalars['Int']['input']>;
  where?: InputMaybe<ProductsWhereInput>;
};


export type QueryProductsTotalArgs = {
  where: ProductsWhereInput;
};


export type QueryReviewsArgs = {
  productId: Scalars['String']['input'];
};

export type Review = {
  content: Scalars['String']['output'];
  createdAt: Scalars['String']['output'];
  email: Scalars['String']['output'];
  id: Scalars['String']['output'];
  name: Scalars['String']['output'];
  product: Product;
  productId: Scalars['String']['output'];
  rating: Scalars['Int']['output'];
  title: Scalars['String']['output'];
};

export type ReviewAddDto = {
  content: Scalars['String']['input'];
  email: Scalars['String']['input'];
  name: Scalars['String']['input'];
  productId: Scalars['String']['input'];
  rating: Scalars['Int']['input'];
  title: Scalars['String']['input'];
};

export type CartCreateMutationVariables = Exact<{ [key: string]: never; }>;


export type CartCreateMutation = { cartCreate?: { id: string, cartItems: Array<{ id: string, quantity: number, product: { id: string, name: string, slug: string, price: number, image: string, category: { name: string } } }> } | null };

export type CartItemAddMutationVariables = Exact<{
  cartId: Scalars['String']['input'];
  productId: Scalars['String']['input'];
}>;


export type CartItemAddMutation = { cartItemAdd: { id: string, quantity: number, product: { id: string, name: string, slug: string, price: number, image: string, category: { name: string } } } };

export type CartItemRemoveMutationVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type CartItemRemoveMutation = { cartItemRemove: { id: string } };

export type CartItemUpdateQuantityMutationVariables = Exact<{
  id: Scalars['String']['input'];
  quantity: Scalars['Int']['input'];
}>;


export type CartItemUpdateQuantityMutation = { cartItemUpdateQuantity: { id: string, quantity: number, product: { id: string, name: string, slug: string, price: number, image: string, category: { name: string } } } };

export type RevievCreateMutationVariables = Exact<{
  data: ReviewAddDto;
}>;


export type RevievCreateMutation = { reviewCreate: { id: string } };

export type CartGetCartItemsListQueryVariables = Exact<{
  cartId: Scalars['String']['input'];
}>;


export type CartGetCartItemsListQuery = { cart?: { id: string, cartItems: Array<{ id: string, quantity: number, product: { id: string, name: string, slug: string, price: number, image: string, category: { name: string } } }> } | null, cartTotals: { productsCount: number, productsTotalPrice: number } };

export type GetCartTotalsQueryVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type GetCartTotalsQuery = { cartTotals: { productsCount: number, productsTotalPrice: number } };

export type CategoryGetListQueryVariables = Exact<{ [key: string]: never; }>;


export type CategoryGetListQuery = { categories: Array<{ id: string, name: string, slug: string }> };

export type CategoryGetOneBySlugQueryVariables = Exact<{
  slug: Scalars['String']['input'];
  where?: InputMaybe<ProductsWhereInput>;
  take?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
}>;


export type CategoryGetOneBySlugQuery = { category: { id: string, slug: string, name: string, products: Array<{ id: string, name: string, slug: string, price: number, image: string, avgRating: number, category: { name: string }, collections: Array<{ name: string }> }> }, productsTotal: { total: number } };

export type CollectionGetListQueryVariables = Exact<{ [key: string]: never; }>;


export type CollectionGetListQuery = { collections: Array<{ id: string, name: string, slug: string }> };

export type CollectionGetOneBySlugQueryVariables = Exact<{
  slug: Scalars['String']['input'];
  where?: InputMaybe<ProductsWhereInput>;
  take?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
}>;


export type CollectionGetOneBySlugQuery = { collection: { id: string, slug: string, name: string, products: Array<{ id: string, name: string, slug: string, price: number, image: string, avgRating: number, category: { name: string }, collections: Array<{ name: string }> }> }, productsTotal: { total: number } };

export type ProductGetByIdQueryVariables = Exact<{
  id: Scalars['String']['input'];
}>;


export type ProductGetByIdQuery = { product: { id: string, name: string, slug: string, description: string, price: number, image: string, category: { id: string, name: string, slug: string }, collections: Array<{ id: string, name: string, slug: string }> } };

export type ProductGetBySlugQueryVariables = Exact<{
  slug: Scalars['String']['input'];
}>;


export type ProductGetBySlugQuery = { productBySlug: { id: string, name: string, slug: string, description: string, price: number, image: string, category: { id: string, name: string, slug: string }, collections: Array<{ id: string, name: string, slug: string }> } };

export type ProductGetListQueryVariables = Exact<{
  where: ProductsWhereInput;
  take?: InputMaybe<Scalars['Int']['input']>;
  skip?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Scalars['String']['input']>;
}>;


export type ProductGetListQuery = { products: Array<{ id: string, name: string, slug: string, price: number, image: string, avgRating: number, category: { name: string }, collections: Array<{ name: string }> }>, productsTotal: { total: number } };

export type ProductSearchQueryQueryVariables = Exact<{
  search: Scalars['String']['input'];
}>;


export type ProductSearchQueryQuery = { products: Array<{ id: string, name: string, slug: string, price: number, image: string, avgRating: number, category: { name: string }, collections: Array<{ name: string }> }>, productsTotal: { total: number } };

export type ReviewsGetListForProductQueryVariables = Exact<{
  productId: Scalars['String']['input'];
}>;


export type ReviewsGetListForProductQuery = { reviews: Array<{ id: string, name: string, email: string, title: string, content: string, rating: number, createdAt: string }> };

export type CartItemProductFragment = { id: string, name: string, slug: string, price: number, image: string, category: { name: string } };

export type ProductItemFragment = { id: string, name: string, slug: string, description: string, price: number, image: string, category: { id: string, name: string, slug: string }, collections: Array<{ id: string, name: string, slug: string }> };

export type ProductListItemFragment = { id: string, name: string, slug: string, price: number, image: string, avgRating: number, category: { name: string }, collections: Array<{ name: string }> };

export type ReviewItemFragment = { id: string, name: string, email: string, title: string, content: string, rating: number, createdAt: string };

export class TypedDocumentString<TResult, TVariables>
  extends String
  implements DocumentTypeDecoration<TResult, TVariables>
{
  __apiType?: DocumentTypeDecoration<TResult, TVariables>['__apiType'];

  constructor(private value: string, public __meta__?: Record<string, any>) {
    super(value);
  }

  toString(): string & DocumentTypeDecoration<TResult, TVariables> {
    return this.value;
  }
}
export const CartItemProductFragmentDoc = new TypedDocumentString(`
    fragment CartItemProduct on Product {
  id
  name
  slug
  price
  image
  category {
    name
  }
}
    `, {"fragmentName":"CartItemProduct"}) as unknown as TypedDocumentString<CartItemProductFragment, unknown>;
export const ProductItemFragmentDoc = new TypedDocumentString(`
    fragment ProductItem on Product {
  id
  name
  slug
  description
  price
  image
  category {
    id
    name
    slug
  }
  collections {
    id
    name
    slug
  }
}
    `, {"fragmentName":"ProductItem"}) as unknown as TypedDocumentString<ProductItemFragment, unknown>;
export const ProductListItemFragmentDoc = new TypedDocumentString(`
    fragment ProductListItem on Product {
  id
  name
  slug
  price
  image
  avgRating
  category {
    name
  }
  collections {
    name
  }
}
    `, {"fragmentName":"ProductListItem"}) as unknown as TypedDocumentString<ProductListItemFragment, unknown>;
export const ReviewItemFragmentDoc = new TypedDocumentString(`
    fragment ReviewItem on Review {
  id
  name
  email
  title
  content
  rating
  createdAt
}
    `, {"fragmentName":"ReviewItem"}) as unknown as TypedDocumentString<ReviewItemFragment, unknown>;
export const CartCreateDocument = new TypedDocumentString(`
    mutation CartCreate {
  cartCreate {
    id
    cartItems {
      id
      quantity
      product {
        ...CartItemProduct
      }
    }
  }
}
    fragment CartItemProduct on Product {
  id
  name
  slug
  price
  image
  category {
    name
  }
}`) as unknown as TypedDocumentString<CartCreateMutation, CartCreateMutationVariables>;
export const CartItemAddDocument = new TypedDocumentString(`
    mutation CartItemAdd($cartId: String!, $productId: String!) {
  cartItemAdd(data: {cartId: $cartId, productId: $productId}) {
    id
    quantity
    product {
      ...CartItemProduct
    }
  }
}
    fragment CartItemProduct on Product {
  id
  name
  slug
  price
  image
  category {
    name
  }
}`) as unknown as TypedDocumentString<CartItemAddMutation, CartItemAddMutationVariables>;
export const CartItemRemoveDocument = new TypedDocumentString(`
    mutation CartItemRemove($id: String!) {
  cartItemRemove(data: {id: $id}) {
    id
  }
}
    `) as unknown as TypedDocumentString<CartItemRemoveMutation, CartItemRemoveMutationVariables>;
export const CartItemUpdateQuantityDocument = new TypedDocumentString(`
    mutation CartItemUpdateQuantity($id: String!, $quantity: Int!) {
  cartItemUpdateQuantity(data: {id: $id, quantity: $quantity}) {
    id
    quantity
    product {
      ...CartItemProduct
    }
  }
}
    fragment CartItemProduct on Product {
  id
  name
  slug
  price
  image
  category {
    name
  }
}`) as unknown as TypedDocumentString<CartItemUpdateQuantityMutation, CartItemUpdateQuantityMutationVariables>;
export const RevievCreateDocument = new TypedDocumentString(`
    mutation RevievCreate($data: ReviewAddDto!) {
  reviewCreate(data: $data) {
    id
  }
}
    `) as unknown as TypedDocumentString<RevievCreateMutation, RevievCreateMutationVariables>;
export const CartGetCartItemsListDocument = new TypedDocumentString(`
    query CartGetCartItemsList($cartId: String!) {
  cart(id: $cartId) {
    id
    cartItems {
      id
      quantity
      product {
        ...CartItemProduct
      }
    }
  }
  cartTotals(id: $cartId) {
    productsCount
    productsTotalPrice
  }
}
    fragment CartItemProduct on Product {
  id
  name
  slug
  price
  image
  category {
    name
  }
}`) as unknown as TypedDocumentString<CartGetCartItemsListQuery, CartGetCartItemsListQueryVariables>;
export const GetCartTotalsDocument = new TypedDocumentString(`
    query GetCartTotals($id: String!) {
  cartTotals(id: $id) {
    productsCount
    productsTotalPrice
  }
}
    `) as unknown as TypedDocumentString<GetCartTotalsQuery, GetCartTotalsQueryVariables>;
export const CategoryGetListDocument = new TypedDocumentString(`
    query CategoryGetList {
  categories {
    id
    name
    slug
  }
}
    `) as unknown as TypedDocumentString<CategoryGetListQuery, CategoryGetListQueryVariables>;
export const CategoryGetOneBySlugDocument = new TypedDocumentString(`
    query CategoryGetOneBySlug($slug: String!, $where: ProductsWhereInput, $take: Int, $skip: Int, $sort: String) {
  category(slug: $slug) {
    id
    slug
    name
    products(where: $where, take: $take, skip: $skip, sort: $sort) {
      ...ProductListItem
    }
  }
  productsTotal(where: {category_slug: $slug}) {
    total
  }
}
    fragment ProductListItem on Product {
  id
  name
  slug
  price
  image
  avgRating
  category {
    name
  }
  collections {
    name
  }
}`) as unknown as TypedDocumentString<CategoryGetOneBySlugQuery, CategoryGetOneBySlugQueryVariables>;
export const CollectionGetListDocument = new TypedDocumentString(`
    query CollectionGetList {
  collections {
    id
    name
    slug
  }
}
    `) as unknown as TypedDocumentString<CollectionGetListQuery, CollectionGetListQueryVariables>;
export const CollectionGetOneBySlugDocument = new TypedDocumentString(`
    query CollectionGetOneBySlug($slug: String!, $where: ProductsWhereInput, $take: Int, $skip: Int, $sort: String) {
  collection(slug: $slug) {
    id
    slug
    name
    products(where: $where, take: $take, skip: $skip, sort: $sort) {
      ...ProductListItem
    }
  }
  productsTotal(where: {collection_slug: $slug}) {
    total
  }
}
    fragment ProductListItem on Product {
  id
  name
  slug
  price
  image
  avgRating
  category {
    name
  }
  collections {
    name
  }
}`) as unknown as TypedDocumentString<CollectionGetOneBySlugQuery, CollectionGetOneBySlugQueryVariables>;
export const ProductGetByIdDocument = new TypedDocumentString(`
    query ProductGetById($id: String!) {
  product(id: $id) {
    ...ProductItem
  }
}
    fragment ProductItem on Product {
  id
  name
  slug
  description
  price
  image
  category {
    id
    name
    slug
  }
  collections {
    id
    name
    slug
  }
}`) as unknown as TypedDocumentString<ProductGetByIdQuery, ProductGetByIdQueryVariables>;
export const ProductGetBySlugDocument = new TypedDocumentString(`
    query ProductGetBySlug($slug: String!) {
  productBySlug(slug: $slug) {
    ...ProductItem
  }
}
    fragment ProductItem on Product {
  id
  name
  slug
  description
  price
  image
  category {
    id
    name
    slug
  }
  collections {
    id
    name
    slug
  }
}`) as unknown as TypedDocumentString<ProductGetBySlugQuery, ProductGetBySlugQueryVariables>;
export const ProductGetListDocument = new TypedDocumentString(`
    query ProductGetList($where: ProductsWhereInput!, $take: Int, $skip: Int, $sort: String) {
  products(where: $where, take: $take, skip: $skip, sort: $sort) {
    ...ProductListItem
  }
  productsTotal(where: $where) {
    total
  }
}
    fragment ProductListItem on Product {
  id
  name
  slug
  price
  image
  avgRating
  category {
    name
  }
  collections {
    name
  }
}`) as unknown as TypedDocumentString<ProductGetListQuery, ProductGetListQueryVariables>;
export const ProductSearchQueryDocument = new TypedDocumentString(`
    query ProductSearchQuery($search: String!) {
  products(where: {name_contains: $search}) {
    ...ProductListItem
  }
  productsTotal(where: {name_contains: $search}) {
    total
  }
}
    fragment ProductListItem on Product {
  id
  name
  slug
  price
  image
  avgRating
  category {
    name
  }
  collections {
    name
  }
}`) as unknown as TypedDocumentString<ProductSearchQueryQuery, ProductSearchQueryQueryVariables>;
export const ReviewsGetListForProductDocument = new TypedDocumentString(`
    query ReviewsGetListForProduct($productId: String!) {
  reviews(productId: $productId) {
    ...ReviewItem
  }
}
    fragment ReviewItem on Review {
  id
  name
  email
  title
  content
  rating
  createdAt
}`) as unknown as TypedDocumentString<ReviewsGetListForProductQuery, ReviewsGetListForProductQueryVariables>;