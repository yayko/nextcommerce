/* eslint-disable */
import * as types from './graphql';



/**
 * Map of all GraphQL operations in the project.
 *
 * This map has several performance disadvantages:
 * 1. It is not tree-shakeable, so it will include all operations in the project.
 * 2. It is not minifiable, so the string of a GraphQL query will be multiple times inside the bundle.
 * 3. It does not support dead code elimination, so it will add unused operations.
 *
 * Therefore it is highly recommended to use the babel or swc plugin for production.
 */
const documents = {
    "mutation CartCreate {\n  cartCreate {\n    id\n    cartItems {\n      id\n      quantity\n      product {\n        ...CartItemProduct\n      }\n    }\n  }\n}": types.CartCreateDocument,
    "mutation CartItemAdd($cartId: String!, $productId: String!) {\n  cartItemAdd(data: {cartId: $cartId, productId: $productId}) {\n    id\n    quantity\n    product {\n      ...CartItemProduct\n    }\n  }\n}": types.CartItemAddDocument,
    "mutation CartItemRemove($id: String!) {\n  cartItemRemove(data: {id: $id}) {\n    id\n  }\n}": types.CartItemRemoveDocument,
    "mutation CartItemUpdateQuantity($id: String!, $quantity: Int!) {\n  cartItemUpdateQuantity(data: {id: $id, quantity: $quantity}) {\n    id\n    quantity\n    product {\n      ...CartItemProduct\n    }\n  }\n}": types.CartItemUpdateQuantityDocument,
    "mutation RevievCreate($data: ReviewAddDto!) {\n  reviewCreate(data: $data) {\n    id\n  }\n}": types.RevievCreateDocument,
    "query CartGetCartItemsList($cartId: String!) {\n  cart(id: $cartId) {\n    id\n    cartItems {\n      id\n      quantity\n      product {\n        ...CartItemProduct\n      }\n    }\n  }\n  cartTotals(id: $cartId) {\n    productsCount\n    productsTotalPrice\n  }\n}": types.CartGetCartItemsListDocument,
    "query GetCartTotals($id: String!) {\n  cartTotals(id: $id) {\n    productsCount\n    productsTotalPrice\n  }\n}": types.GetCartTotalsDocument,
    "query CategoryGetList {\n  categories {\n    id\n    name\n    slug\n  }\n}": types.CategoryGetListDocument,
    "query CategoryGetOneBySlug($slug: String!, $where: ProductsWhereInput, $take: Int, $skip: Int, $sort: String) {\n  category(slug: $slug) {\n    id\n    slug\n    name\n    products(where: $where, take: $take, skip: $skip, sort: $sort) {\n      ...ProductListItem\n    }\n  }\n  productsTotal(where: {category_slug: $slug}) {\n    total\n  }\n}": types.CategoryGetOneBySlugDocument,
    "query CollectionGetList {\n  collections {\n    id\n    name\n    slug\n  }\n}": types.CollectionGetListDocument,
    "query CollectionGetOneBySlug($slug: String!, $where: ProductsWhereInput, $take: Int, $skip: Int, $sort: String) {\n  collection(slug: $slug) {\n    id\n    slug\n    name\n    products(where: $where, take: $take, skip: $skip, sort: $sort) {\n      ...ProductListItem\n    }\n  }\n  productsTotal(where: {collection_slug: $slug}) {\n    total\n  }\n}": types.CollectionGetOneBySlugDocument,
    "query ProductGetById($id: String!) {\n  product(id: $id) {\n    ...ProductItem\n  }\n}": types.ProductGetByIdDocument,
    "query ProductGetBySlug($slug: String!) {\n  productBySlug(slug: $slug) {\n    ...ProductItem\n  }\n}": types.ProductGetBySlugDocument,
    "query ProductGetList($where: ProductsWhereInput!, $take: Int, $skip: Int, $sort: String) {\n  products(where: $where, take: $take, skip: $skip, sort: $sort) {\n    ...ProductListItem\n  }\n  productsTotal(where: $where) {\n    total\n  }\n}": types.ProductGetListDocument,
    "query ProductSearchQuery($search: String!) {\n  products(where: {name_contains: $search}) {\n    ...ProductListItem\n  }\n  productsTotal(where: {name_contains: $search}) {\n    total\n  }\n}": types.ProductSearchQueryDocument,
    "query ReviewsGetListForProduct($productId: String!) {\n  reviews(productId: $productId) {\n    ...ReviewItem\n  }\n}": types.ReviewsGetListForProductDocument,
    "fragment CartItemProduct on Product {\n  id\n  name\n  slug\n  price\n  image\n  category {\n    name\n  }\n}": types.CartItemProductFragmentDoc,
    "fragment ProductItem on Product {\n  id\n  name\n  slug\n  description\n  price\n  image\n  category {\n    id\n    name\n    slug\n  }\n  collections {\n    id\n    name\n    slug\n  }\n}": types.ProductItemFragmentDoc,
    "fragment ProductListItem on Product {\n  id\n  name\n  slug\n  price\n  image\n  avgRating\n  category {\n    name\n  }\n  collections {\n    name\n  }\n}": types.ProductListItemFragmentDoc,
    "fragment ReviewItem on Review {\n  id\n  name\n  email\n  title\n  content\n  rating\n  createdAt\n}": types.ReviewItemFragmentDoc,
};

/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "mutation CartCreate {\n  cartCreate {\n    id\n    cartItems {\n      id\n      quantity\n      product {\n        ...CartItemProduct\n      }\n    }\n  }\n}"): typeof import('./graphql').CartCreateDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "mutation CartItemAdd($cartId: String!, $productId: String!) {\n  cartItemAdd(data: {cartId: $cartId, productId: $productId}) {\n    id\n    quantity\n    product {\n      ...CartItemProduct\n    }\n  }\n}"): typeof import('./graphql').CartItemAddDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "mutation CartItemRemove($id: String!) {\n  cartItemRemove(data: {id: $id}) {\n    id\n  }\n}"): typeof import('./graphql').CartItemRemoveDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "mutation CartItemUpdateQuantity($id: String!, $quantity: Int!) {\n  cartItemUpdateQuantity(data: {id: $id, quantity: $quantity}) {\n    id\n    quantity\n    product {\n      ...CartItemProduct\n    }\n  }\n}"): typeof import('./graphql').CartItemUpdateQuantityDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "mutation RevievCreate($data: ReviewAddDto!) {\n  reviewCreate(data: $data) {\n    id\n  }\n}"): typeof import('./graphql').RevievCreateDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "query CartGetCartItemsList($cartId: String!) {\n  cart(id: $cartId) {\n    id\n    cartItems {\n      id\n      quantity\n      product {\n        ...CartItemProduct\n      }\n    }\n  }\n  cartTotals(id: $cartId) {\n    productsCount\n    productsTotalPrice\n  }\n}"): typeof import('./graphql').CartGetCartItemsListDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "query GetCartTotals($id: String!) {\n  cartTotals(id: $id) {\n    productsCount\n    productsTotalPrice\n  }\n}"): typeof import('./graphql').GetCartTotalsDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "query CategoryGetList {\n  categories {\n    id\n    name\n    slug\n  }\n}"): typeof import('./graphql').CategoryGetListDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "query CategoryGetOneBySlug($slug: String!, $where: ProductsWhereInput, $take: Int, $skip: Int, $sort: String) {\n  category(slug: $slug) {\n    id\n    slug\n    name\n    products(where: $where, take: $take, skip: $skip, sort: $sort) {\n      ...ProductListItem\n    }\n  }\n  productsTotal(where: {category_slug: $slug}) {\n    total\n  }\n}"): typeof import('./graphql').CategoryGetOneBySlugDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "query CollectionGetList {\n  collections {\n    id\n    name\n    slug\n  }\n}"): typeof import('./graphql').CollectionGetListDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "query CollectionGetOneBySlug($slug: String!, $where: ProductsWhereInput, $take: Int, $skip: Int, $sort: String) {\n  collection(slug: $slug) {\n    id\n    slug\n    name\n    products(where: $where, take: $take, skip: $skip, sort: $sort) {\n      ...ProductListItem\n    }\n  }\n  productsTotal(where: {collection_slug: $slug}) {\n    total\n  }\n}"): typeof import('./graphql').CollectionGetOneBySlugDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "query ProductGetById($id: String!) {\n  product(id: $id) {\n    ...ProductItem\n  }\n}"): typeof import('./graphql').ProductGetByIdDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "query ProductGetBySlug($slug: String!) {\n  productBySlug(slug: $slug) {\n    ...ProductItem\n  }\n}"): typeof import('./graphql').ProductGetBySlugDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "query ProductGetList($where: ProductsWhereInput!, $take: Int, $skip: Int, $sort: String) {\n  products(where: $where, take: $take, skip: $skip, sort: $sort) {\n    ...ProductListItem\n  }\n  productsTotal(where: $where) {\n    total\n  }\n}"): typeof import('./graphql').ProductGetListDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "query ProductSearchQuery($search: String!) {\n  products(where: {name_contains: $search}) {\n    ...ProductListItem\n  }\n  productsTotal(where: {name_contains: $search}) {\n    total\n  }\n}"): typeof import('./graphql').ProductSearchQueryDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "query ReviewsGetListForProduct($productId: String!) {\n  reviews(productId: $productId) {\n    ...ReviewItem\n  }\n}"): typeof import('./graphql').ReviewsGetListForProductDocument;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "fragment CartItemProduct on Product {\n  id\n  name\n  slug\n  price\n  image\n  category {\n    name\n  }\n}"): typeof import('./graphql').CartItemProductFragmentDoc;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "fragment ProductItem on Product {\n  id\n  name\n  slug\n  description\n  price\n  image\n  category {\n    id\n    name\n    slug\n  }\n  collections {\n    id\n    name\n    slug\n  }\n}"): typeof import('./graphql').ProductItemFragmentDoc;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "fragment ProductListItem on Product {\n  id\n  name\n  slug\n  price\n  image\n  avgRating\n  category {\n    name\n  }\n  collections {\n    name\n  }\n}"): typeof import('./graphql').ProductListItemFragmentDoc;
/**
 * The graphql function is used to parse GraphQL queries into a document that can be used by GraphQL clients.
 */
export function graphql(source: "fragment ReviewItem on Review {\n  id\n  name\n  email\n  title\n  content\n  rating\n  createdAt\n}"): typeof import('./graphql').ReviewItemFragmentDoc;


export function graphql(source: string) {
  return (documents as any)[source] ?? {};
}
