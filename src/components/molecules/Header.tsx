import Image from "next/image";
import Link from "next/link";
import { ProductSearchInput } from "./ProductSearchInput";
import { type HeaderProps } from "@/types";
import { CartHeaderLink } from "@/components/atoms/cart/CartHeaderLink";
import { MobileMenu } from "@/components/atoms/MobileMenu";
import { NavigationLinks } from "@/components/atoms/NavigationLinks";

export const Header = (props: HeaderProps) => {
	return (
		<header className="sticky top-0 z-10 border-b-2 bg-neutral-800 text-gray-200">
			<div className="mx-auto hidden max-w-7xl justify-center self-center px-4 lg:flex">
				<nav className="flex">
					<Link href="/" className="me-8 self-center">
						<Image src="/logo.png" width={120} height={45} alt="Logo" />
					</Link>
					<NavigationLinks {...props} />
				</nav>
				<ProductSearchInput />
				<CartHeaderLink />
			</div>

			<div className="flex min-h-[70px] justify-between px-4 lg:hidden">
				<Link href="/" className="me-8 self-center">
					<Image src="/logo.png" width={120} height={45} alt="Logo" />
				</Link>
				<div className="flex">
					<CartHeaderLink />
					<MobileMenu {...props} />
				</div>
			</div>
		</header>
	);
};
