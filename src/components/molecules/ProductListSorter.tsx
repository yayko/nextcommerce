"use client";
import { useRouter, useSearchParams } from "next/navigation";
import { type Route } from "next";
import { getUpdatedQueryString } from "@/utils/queryParamsUtils";

export const ProductListSorter = ({ urlPrefix }: { urlPrefix: string }) => {
	const router = useRouter();
	const searchParams = useSearchParams();
	const queryString = searchParams.toString();
	const sorts = {
		price: "Price: Low to High",
		"-price": "Price: High to Low",
		name: "Name: A to Z",
		"-name": "Name: Z to A",
		avgRating: "Rating: Low to High",
		"-avgRating": "Rating: High to Low",
	};

	const dropdownOptions = Object.entries(sorts).map(([value, label]) => (
		<option
			key={value}
			value={value}
			data-testid={`sort-by-${value.replace("-", "").replace("avgRating", "rating")}`}
		>
			{label}
		</option>
	));

	return (
		<>
			<h4 className="mb-4 border-b-2 border-b-gray-400 pb-4 font-bold md:text-xl">Sort by</h4>
			<select
				name="sort"
				className="w-full py-2 pe-4"
				onChange={(e) => {
					router.push(
						`${urlPrefix}?${getUpdatedQueryString(queryString, "sort", e.target.value)}` as Route,
					);
				}}
				defaultValue={searchParams.get("sort") || "name"}
			>
				{dropdownOptions}
			</select>
		</>
	);
};
