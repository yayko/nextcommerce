"use client";
import { experimental_useOptimistic as useOptimistic } from "react";
import { IncrementQuantityButton } from "@/components/atoms/cart/IncrementQuantityButton";
import { type CartItem } from "@/gql/graphql";
import { updateCartItemQuantityAction } from "@/actions/cartActions";
import { DecrementQuantityButton } from "@/components/atoms/cart/DecrementQuantityButton";

export const UpdateCartItemQuantityInput = ({
	cartItem: { quantity, id },
}: {
	cartItem: Pick<CartItem, "quantity" | "id">;
}) => {
	const [optimisticQuantity, setOptimisticQuantity] = useOptimistic(
		quantity,
		(_, nextQuantity: number) => nextQuantity,
	);

	const incrementQuantity = async () => {
		setOptimisticQuantity(optimisticQuantity + 1);
		await updateCartItemQuantityAction(id, optimisticQuantity + 1);
	};

	const decrementQuantity = async () => {
		setOptimisticQuantity(optimisticQuantity - 1);
		await updateCartItemQuantityAction(id, optimisticQuantity - 1);
	};

	return (
		<form className="flex h-10 w-[150px]">
			<DecrementQuantityButton onDecrement={decrementQuantity} disabled={optimisticQuantity <= 1} />
			<div
				data-testid="quantity"
				className="flex h-full grow items-center justify-center border-b border-t border-slate-200 text-center"
			>
				{optimisticQuantity}
			</div>
			<IncrementQuantityButton onIncrement={incrementQuantity} disabled={false} />
		</form>
	);
};
