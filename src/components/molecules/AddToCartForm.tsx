import { addToCartAction } from "@/actions/cartActions";
import { AddToCartButton } from "@/components/atoms/cart/AddToCartButton";

export const AddToCartForm = async ({ productId }: { productId: string }) => {
	return (
		<form action={addToCartAction}>
			<input type="hidden" name="productId" value={productId} readOnly />
			<AddToCartButton />
		</form>
	);
};
