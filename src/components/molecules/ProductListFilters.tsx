"use client";
import { useRouter, useSearchParams } from "next/navigation";
import { getUpdatedQueryString } from "@/utils/queryParamsUtils";

export const ProductListFilters = () => {
	const router = useRouter();
	const searchParams = useSearchParams();
	const queryString = searchParams.toString();
	const sorts = {
		price: "Price: Low to High",
		"-price": "Price: High to Low",
		name: "Name: A to Z",
		"-name": "Name: Z to A",
		avgRating: "Rating: Low to High",
		"-avgRating": "Rating: High to Low",
	};

	const dropdownOptions = Object.entries(sorts).map(([value, label]) => (
		<option key={value} value={value}>
			{label}
		</option>
	));

	return (
		<>
			<h4 className="mb-4 border-b-2 border-b-gray-400 pb-4 font-bold md:text-xl">Sort by</h4>
			<select
				name="sort"
				id="sort-by-price"
				className="w-full py-2 pe-4 ps-2"
				onChange={(e) => {
					router.push(`/products?${getUpdatedQueryString(queryString, "sort", e.target.value)}`);
				}}
				defaultValue={searchParams.get("sort") || "name"}
			>
				{dropdownOptions}
			</select>
		</>
	);
};
