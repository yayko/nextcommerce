import { type Route } from "next";
import { ProductListItemImage } from "@/components/atoms/product/ProductListItemImage";
import { ProductListItemDescription } from "@/components/atoms/product/ProductListItemDescription";
import type { ProductListItemProps } from "@/types";

export const ProductListItem = ({
	product: { slug, image, name, price, avgRating },
	categoryName,
}: ProductListItemProps) => {
	return (
		<li>
			<article className="mb-5">
				<ProductListItemImage
					src={image}
					alt={name}
					url={`/product/${slug}` as Route}
					width={768}
					height={768}
				/>
				<ProductListItemDescription
					slug={slug}
					title={name}
					price={price}
					category={categoryName}
					avgRating={avgRating}
				/>
			</article>
		</li>
	);
};
