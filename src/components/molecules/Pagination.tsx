import clsx from "clsx";
import { type Route } from "next";
import { ChevronLeft, ChevronRight } from "lucide-react";
import { ActiveLink } from "@/components/atoms/ActiveLink";

interface PaginationProps {
	currentPage: number;
	totalPages: number;
	urlPrefix: string;
	searchParams?: Record<string, string>;
}

const getStartPage = (currentPage: number, totalPages: number, showItemsCount: number) => {
	const itemsCountHalfRounded = Math.ceil(showItemsCount / 2);

	switch (true) {
		case totalPages <= showItemsCount:
			return 0;
		case totalPages - currentPage < itemsCountHalfRounded:
			return totalPages - showItemsCount;
		case currentPage < itemsCountHalfRounded:
			return 0;
		default:
			return currentPage - itemsCountHalfRounded;
	}
};

const getEndPage = (currentPage: number, showItemsCount: number) => {
	const itemsCountHalfRounded = Math.ceil(showItemsCount / 2);
	return currentPage < itemsCountHalfRounded
		? showItemsCount
		: currentPage + itemsCountHalfRounded - 1;
};

export const Pagination = ({
	currentPage,
	totalPages,
	urlPrefix,
	searchParams,
}: PaginationProps) => {
	const start = getStartPage(currentPage, totalPages, 5);
	const end = getEndPage(currentPage, 5);
	const queryString = searchParams ? `?${new URLSearchParams(searchParams).toString()}` : "";

	const paginationGroup = Array.from({ length: totalPages })
		.map((v, k) => k + 1)
		.slice(start, end);

	const baseClass = "inline-block h-10 w-10 rounded-sm p-2";

	return totalPages > 1 ? (
		<div className="text-center">
			<ul className="mt-8 flex justify-center gap-1.5" aria-label="pagination">
				{paginationGroup.length && currentPage > 1 ? (
					<li key={"prev-page"} className="page-item">
						<ActiveLink
							className={baseClass}
							href={`${urlPrefix}/${currentPage - 1}${queryString}` as Route}
						>
							<ChevronLeft />
						</ActiveLink>
					</li>
				) : (
					<span className={clsx(baseClass, "opacity-30")}>
						<ChevronLeft />
					</span>
				)}

				{paginationGroup.map((pageNumber) => {
					return (
						<li key={`page-${pageNumber}`} className="h-full shadow-xl">
							<ActiveLink
								href={`${urlPrefix}/${pageNumber}${queryString}` as Route}
								className={clsx(
									baseClass,
									" bg-red-500 p-2 text-white transition-colors hover:bg-red-800",
									{
										"bg-red-700": currentPage == pageNumber,
									},
								)}
							>
								{pageNumber}
							</ActiveLink>
						</li>
					);
				})}

				{paginationGroup.length && currentPage !== totalPages ? (
					<li key={"next-page"} className="page-item">
						{currentPage < totalPages ? (
							<ActiveLink
								className={baseClass}
								href={`${urlPrefix}/${currentPage + 1}${queryString}` as Route}
							>
								<ChevronRight />
							</ActiveLink>
						) : (
							""
						)}
					</li>
				) : (
					<span className={clsx(baseClass, "opacity-30")}>
						<ChevronRight />
					</span>
				)}
			</ul>

			{!paginationGroup.length ? null : (
				<p className="mt-4">
					Show {currentPage} of {totalPages} total pages
				</p>
			)}
		</div>
	) : null;
};
