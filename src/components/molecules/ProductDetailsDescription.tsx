import { formatPrice } from "@/utils/productUtils";
import { type Product } from "@/types";
import { AddToCartForm } from "@/components/molecules/AddToCartForm";

export const ProductDetailsDescription = ({
	id,
	name,
	price,
	description,
}: Pick<Product, "id" | "name" | "description" | "price">) => {
	return (
		<div className="basis-1-1 p-8 pt-0 sm:basis-1/2">
			<h1 className="mb-4 text-2xl font-bold md:text-3xl">{name}</h1>
			<p className="mb-8 text-3xl font-semibold text-red-500 md:text-5xl">{formatPrice(price)}</p>
			<p>{description}</p>
			<AddToCartForm productId={id} />
		</div>
	);
};
