import { ProductListItem } from "@/components/molecules/ProductListItem";
import type { ProductListProps } from "@/types";

export const ProductList = ({ products }: ProductListProps) => (
	<ul className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3" data-testid="products-list">
		{products.map((product) => (
			<ProductListItem key={product.id} product={product} categoryName={product.category.name} />
		))}
	</ul>
);
