import { getProducts } from "@/api/productAPI";
import { SectionTitle } from "@/components/atoms/SectionTitle";
import { ProductListItem } from "@/components/molecules/ProductListItem";

export const SuggestedProductsList = async ({ title }: { title?: string }) => {
	const { products } = await getProducts({
		params: { take: 4, page: 1 },
		searchParams: { sort: "-name" },
	});
	// shuffleArray(products);

	return (
		<div data-testid="related-products">
			<SectionTitle>{title}</SectionTitle>
			<ul
				className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4"
				data-testid="products-list"
			>
				{products.map((product) => (
					<ProductListItem
						key={product.id}
						product={product}
						categoryName={product.category.name}
					/>
				))}
			</ul>
		</div>
	);
};
