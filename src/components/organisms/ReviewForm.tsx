"use client";
import { addReview } from "@/actions/reviewActions";
import { Button } from "@/components/atoms/Button";
import { LabeledInput } from "@/components/atoms/LabeledInput";
import { ReviewRatingInput } from "@/components/molecules/ReviewRatingInput";

export const ReviewForm = ({ productId }: { productId: string }) => {
	return (
		<form action={addReview} data-testid="add-review-form">
			<input type="hidden" name="productId" defaultValue={productId} readOnly />
			<LabeledInput label="Rate">
				<ReviewRatingInput />
			</LabeledInput>
			<LabeledInput label="Title">
				<input
					type="text"
					name="headline"
					className="w-full rounded border border-slate-200 p-2 focus:outline-amber-300"
					required
				/>
			</LabeledInput>
			<LabeledInput label="Content">
				<textarea
					name="content"
					rows={5}
					className="w-full rounded border border-slate-200 p-2"
					required
				/>
			</LabeledInput>
			<div className="block gap-4 md:flex">
				<div className="md:w-1/2">
					<LabeledInput label="Name">
						<input
							type="text"
							name="name"
							className="w-full rounded border border-slate-200 p-2"
							required
						/>
					</LabeledInput>
				</div>
				<div className="md:w-1/2">
					<LabeledInput label="Email">
						<input
							type="email"
							name="email"
							className="w-full rounded border border-slate-200 p-2"
							required
						/>
					</LabeledInput>
				</div>
			</div>
			<Button type="submit">Submit review</Button>
		</form>
	);
};
