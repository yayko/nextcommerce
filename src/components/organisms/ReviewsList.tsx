import { getReviews } from "@/api/reviewAPI";
import { SectionTitle } from "@/components/atoms/SectionTitle";
import { Review } from "@/components/molecules/Review";
import { ReviewForm } from "@/components/organisms/ReviewForm";

export const ReviewsList = async ({ productId }: { productId: string }) => {
	const { reviews } = await getReviews(productId);

	return (
		<section className="mt-8">
			<SectionTitle>Product reviews</SectionTitle>

			<div className="block md:flex">
				<div className="lg:md-16 me-0 md:me-8 md:w-1/2">
					<ReviewForm productId={productId} />
				</div>
				<div className="md:w-1/2">
					{reviews.map((rev) => (
						<Review key={rev.id} review={rev} />
					))}
				</div>
			</div>
		</section>
	);
};
