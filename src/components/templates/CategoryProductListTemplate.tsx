import { getCategory } from "@/api/categoryAPI";
import { Pagination } from "@/components/molecules/Pagination";
import { ProductListSorter } from "@/components/molecules/ProductListSorter";
import { ProductList } from "@/components/organisms/ProductList";
import type { PageParamsWithSlug } from "@/types";
import { defaultPageSize } from "@/utils/paginationUtils";

export const CategoryProductListTemplate = async (props: PageParamsWithSlug) => {
	const {
		category: { name, products, slug },
		productsTotal,
	} = await getCategory(props);

	return (
		<>
			<h1 className="mb-8 rounded-md bg-red-500 p-4 text-xl text-white">
				Category: <strong>{name}</strong>
			</h1>
			<div className="flex flex-col lg:flex-row">
				<div className="order-1 lg:order-none">
					<ProductList products={products} />
				</div>
				<div className="mb-8 ms-0 min-w-[250px] lg:mb-0 lg:ms-8">
					<ProductListSorter urlPrefix={`/categories/${slug}`} />
				</div>
			</div>
			<Pagination
				urlPrefix={`/categories/${slug}`}
				searchParams={props.searchParams}
				currentPage={+(props.params.page || 1)}
				totalPages={Math.round(productsTotal.total / defaultPageSize)}
			/>
		</>
	);
};
