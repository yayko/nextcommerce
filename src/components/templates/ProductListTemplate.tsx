import { ProductList } from "@/components/organisms/ProductList";
import { Pagination } from "@/components/molecules/Pagination";
import { defaultPageSize } from "@/utils/paginationUtils";
import { getProducts } from "@/api/productAPI";
import { ProductListSorter } from "@/components/molecules/ProductListSorter";
import { type PageParams } from "@/types";

export const ProductListTemplate = async (props: PageParams) => {
	const { products, productsTotal } = await getProducts({ ...props });

	return (
		<>
			<div className="flex flex-col lg:flex-row">
				<div className="order-1 lg:order-none">
					<ProductList products={products} />
				</div>
				<div className="mb-8 ms-0 min-w-[250px] lg:mb-0 lg:ms-8">
					<ProductListSorter urlPrefix={`/products`} />
				</div>
			</div>
			<Pagination
				urlPrefix="/products"
				searchParams={props.searchParams}
				currentPage={+(props.params.page || 1)}
				totalPages={Math.round(productsTotal.total / defaultPageSize)}
			/>
		</>
	);
};
