import { type PropsWithChildren } from "react";

export const SectionTitle = ({ children }: PropsWithChildren) => {
	return <h4 className="mb-8 text-2xl font-bold md:text-3xl">{children}</h4>;
};
