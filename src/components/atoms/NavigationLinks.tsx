import { useCallback } from "react";
import clsx from "clsx";
import { type HeaderProps } from "@/types";
import { ActiveLink } from "@/components/atoms/ActiveLink";

export const NavigationLinks = ({ links, linkClassName, activeLinkClassName }: HeaderProps) => {
	const renderLinks = useCallback(
		() =>
			links.map(({ href, title, activeIfStartsWith }, idx) => {
				return (
					<li
						key={title}
						className={clsx("py-2", {
							"lg:px-4": idx > 0,
							"lg:pr-4": idx === 0,
							"lg:pl-4": idx === links.length - 1,
						})}
					>
						<ActiveLink
							href={href}
							activeClassName={activeLinkClassName}
							className={linkClassName}
							activeIfStartsWith={activeIfStartsWith}
						>
							{title}
						</ActiveLink>
					</li>
				);
			}),
		[activeLinkClassName, linkClassName, links],
	);

	return <ul className="block px-4 pb-4 lg:flex lg:px-0 lg:py-4 ">{renderLinks()}</ul>;
};
