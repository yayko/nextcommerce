import clsx from "clsx";
import { Star } from "lucide-react";
import { type HTMLAttributes } from "react";

export const RatingStar = ({
	filled = true,
	size = 20,
	...spanProps
}: {
	filled: boolean;
	size?: number;
} & HTMLAttributes<HTMLSpanElement>) => (
	<span {...spanProps}>
		<Star size={size} className={clsx(" text-amber-400", { "fill-amber-400": filled })} />
	</span>
);
