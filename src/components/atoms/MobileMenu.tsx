"use client";
import { Menu, X } from "lucide-react";
import { useState } from "react";
import clsx from "clsx";
import Link from "next/link";
import Image from "next/image";
import { type HeaderProps } from "@/types";
import { NavigationLinks } from "@/components/atoms/NavigationLinks";
import { ProductSearchInput } from "@/components/molecules/ProductSearchInput";

export const MobileMenu = (props: HeaderProps) => {
	const [isShown, setIsShown] = useState(false);
	return (
		<div className="self-center">
			<button aria-label="Open menu" onClick={() => setIsShown(!isShown)}>
				<Menu className="relative top-[4px] h-8 w-8" />
			</button>
			<div
				className={clsx("fixed left-0 top-0 h-full w-full bg-white bg-opacity-50", {
					block: isShown,
					hidden: !isShown,
				})}
				onClick={() => setIsShown(false)}
			/>
			<div
				className={clsx(
					"fixed top-0 h-full w-[320px] bg-neutral-800 shadow-md transition-all duration-300",
					{
						"left-0": isShown,
						"-left-[320px]": !isShown,
					},
				)}
			>
				<div className="flex h-[70px] items-center justify-between px-4 ">
					<Link href="/" className="me-8">
						<Image src="/logo.png" width={120} height={45} alt="Logo" />
					</Link>
					<button onClick={() => setIsShown(false)}>
						<X className="h-8 w-8" />
					</button>
				</div>
				<div>
					<ProductSearchInput />
					<NavigationLinks {...props} />
				</div>
			</div>
		</div>
	);
};
