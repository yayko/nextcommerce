"use client";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { type Collection } from "@/types";

export const CollectionsList = ({ collections }: { collections: Collection[] }) => {
	const pathname = usePathname();
	const isHomePage = pathname === "/";
	return isHomePage ? (
		<div className="grid grid-cols-2 gap-x-[2px] text-center shadow-lg">
			{collections.map((collection) => (
				<Link
					key={collection.slug}
					href={`/collections/${collection.slug}`}
					className={`bg-backdrop inline-block rounded-sm bg-${collection.slug}`}
				>
					<span className="transition">{collection.name}</span>
				</Link>
			))}
		</div>
	) : (
		<></>
	);
};
