"use client";

import { usePathname } from "next/navigation";
import Link from "next/link";
import clsx from "clsx";
import type { ActiveLinkProps } from "@/types";

export const ActiveLink = <T extends string>({
	activeClassName,
	className,
	children,
	href,
	activeIfStartsWith,
	...props
}: ActiveLinkProps<T>) => {
	const currentPathname = usePathname();
	const homePathname = "/";
	const isHomePage = href === homePathname;
	const hrefMatchesHome = isHomePage && currentPathname === homePathname;

	const isStartsWithGivenString = activeIfStartsWith
		? currentPathname.startsWith(`${activeIfStartsWith}`)
		: false;

	const isActive =
		hrefMatchesHome ||
		(!isHomePage && currentPathname.startsWith(`${href}`)) ||
		isStartsWithGivenString;
	const cssClass = clsx(className as string, (isActive && activeClassName) as string);

	return (
		<Link href={href} className={cssClass} {...props} aria-current={isActive || undefined}>
			{children}
		</Link>
	);
};
