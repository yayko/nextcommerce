import clsx from "clsx";
import { type ButtonHTMLAttributes, type DetailedHTMLProps } from "react";

export const Button = (
	props: DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>,
) => {
	return (
		<button
			{...props}
			className={clsx(
				"button-bg mt-8 flex min-w-full items-center justify-center rounded-md px-2 py-3 text-white transition-all disabled:cursor-wait disabled:bg-blue-400 disabled:hover:bg-blue-400",
				props.className,
			)}
		>
			{props.children}
		</button>
	);
};
