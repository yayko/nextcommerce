"use client";

import Link from "next/link";
import clsx from "clsx";
import type { ActiveFilterLinkProps } from "@/types";

export const ActiveFilterLink = <T extends string>({
	activeClassName = "font-bold text-blue-600",
	className,
	children,
	isActive,
	href,
	...props
}: ActiveFilterLinkProps<T>) => {
	const cssClass = clsx("block", className, isActive && activeClassName);

	return (
		<Link href={href} className={cssClass} {...props} aria-current={isActive || undefined}>
			{children}
		</Link>
	);
};
