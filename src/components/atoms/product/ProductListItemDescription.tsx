import Link from "next/link";
import type { ProductListItemDescriptionProps } from "@/types";
import { formatPrice } from "@/utils/productUtils";
import { RatingStar } from "@/components/atoms/RatingStar";

export const ProductListItemDescription = ({
	slug,
	title,
	price,
	category,
	avgRating,
}: ProductListItemDescriptionProps) => {
	return (
		<div className="product-list-item-description">
			<div className="mb-2 flex justify-between">
				<span>{category}</span>

				<div className="flex items-center">
					<div className="me-2">
						<span data-testid="product-rating">{avgRating?.toFixed(1)}</span> / 5
					</div>
					{Array.from({ length: 5 }).map((_, idx) => (
						<RatingStar
							size={16}
							className="m-0 p-0"
							key={`star-${idx}`}
							filled={idx + 1 <= avgRating}
						/>
					))}
				</div>
			</div>

			<div className="flex justify-between">
				<Link href={`/product/${slug}`}>
					<h3 className="grow font-semibold transition hover:text-red-600" role="heading">
						{title}
					</h3>
				</Link>
				<span className="shrink-0 font-bold text-red-500" data-testid="product-price">
					{formatPrice(price)}
				</span>
			</div>
		</div>
	);
};
