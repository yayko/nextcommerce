import Image from "next/image";
import { type ProductImageProps } from "@/types";

export const ProductDetailsImage = ({ src, alt, height, width }: ProductImageProps) => (
	<div className="basis-1/1 mb-8 overflow-hidden sm:mb-0 sm:basis-1/2">
		<Image
			alt={alt || ""}
			src={src}
			width={1000 || width}
			height={1000 || height}
			className="mx-auto transition-transform hover:scale-105"
		/>
	</div>
);
