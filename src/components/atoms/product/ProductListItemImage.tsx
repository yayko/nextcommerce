import Image from "next/image";
import type { ProductImageProps } from "@/types";

export const ProductListItemImage = ({ src, alt, width, height }: ProductImageProps) => {
	return (
		<div className="border-gray-150 mx-auto mb-4 overflow-hidden rounded-sm border-gray-100 bg-gray-100 shadow-xl transition-shadow">
			<Image
				width={width || 500}
				height={height || 500}
				src={src}
				alt={alt || ""}
				className="mx-auto h-full w-full object-cover object-center transition-transform hover:scale-105"
			/>
		</div>
	);
};
