"use client";

import { experimental_useFormStatus as useFormStatus } from "react-dom";
import { Spinner } from "@/components/atoms/Spinners";
import { Button } from "@/components/atoms/Button";

export const AddToCartButton = () => {
	const { pending } = useFormStatus();
	return (
		<Button data-testid="add-to-cart-button" type="submit" disabled={pending}>
			<span className="inline-block ">Add to cart</span> {pending && <Spinner />}
		</Button>
	);
};
