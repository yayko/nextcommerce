"use client";

import { experimental_useFormStatus as useFormStatus } from "react-dom";
import { removeCartItemAction } from "@/actions/cartActions";
import { Button } from "@/components/atoms/Button";

export const CartItemRemoveButton = ({ cartItemId }: { cartItemId: string }) => {
	const { pending } = useFormStatus();
	return (
		<form>
			<Button
				className="mt-0 h-10 rounded bg-gray-500 px-4 py-2 text-white transition-colors hover:bg-gray-700	"
				formAction={async () => {
					await removeCartItemAction(cartItemId);
				}}
			>
				Remove {pending && " is pending"}
			</Button>
		</form>
	);
};
