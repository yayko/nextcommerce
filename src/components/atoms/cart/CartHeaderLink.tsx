import { ShoppingBag } from "lucide-react";
import Link from "next/link";
import { cookies } from "next/headers";
import { formatPrice } from "@/utils/productUtils";
import { executeGraphQl } from "@/api/baseGraphQLRequest";
import { CartGetCartItemsListDocument } from "@/gql/graphql";

export const CartHeaderLink = async () => {
	const cartId = cookies().get("cartId")?.value;

	const {
		cartTotals: { productsCount, productsTotalPrice },
	} = cartId
		? await executeGraphQl({
				query: CartGetCartItemsListDocument,
				variables: {
					cartId,
				},
		  })
		: {
				cartTotals: { productsCount: 0, productsTotalPrice: 0 },
		  };

	return (
		<div className="self-center pe-4 ps-4 lg:pe-0">
			<Link href={"/cart"}>
				<div id="cart-total" className="flex w-28 justify-end">
					{formatPrice(productsTotalPrice)}
					<span className="relative ms-2 inline-block">
						<ShoppingBag />
						{productsCount > 0 && (
							<span className="bottom absolute -bottom-3 -left-2 inline-block h-5 w-5 rounded-full bg-red-500 text-center text-sm text-white">
								{productsCount}
							</span>
						)}
					</span>
				</div>
			</Link>
		</div>
	);
};
