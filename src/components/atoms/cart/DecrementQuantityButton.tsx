import { type DecrementQuantityButtonProps } from "@/types";

export const DecrementQuantityButton = ({
	onDecrement,
	disabled,
}: DecrementQuantityButtonProps) => {
	return (
		<button
			data-testid="decrement"
			formAction={onDecrement}
			disabled={disabled}
			className="flex h-full w-10 items-center justify-center rounded-s-md bg-red-500 text-center font-bold text-white transition-colors hover:bg-red-600 disabled:bg-red-300 disabled:hover:cursor-not-allowed"
		>
			-
		</button>
	);
};
