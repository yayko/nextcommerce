import { type IncrementQuantityButtonProps } from "@/types";

export const IncrementQuantityButton = ({
	onIncrement,
	disabled,
}: IncrementQuantityButtonProps) => {
	return (
		<button
			data-testid="increment"
			formAction={onIncrement}
			disabled={disabled}
			className="flex h-full w-10 items-center justify-center rounded-e-md bg-red-500 text-center font-bold text-white transition-colors hover:bg-red-600 disabled:bg-red-300 disabled:hover:cursor-not-allowed"
		>
			+
		</button>
	);
};
