export const Wave = () => {
	return (
		<svg
			id="wave"
			viewBox="0 0 1440 100"
			version="1.1"
			xmlns="http://www.w3.org/2000/svg"
			className="relative -bottom-1"
		>
			<defs>
				<linearGradient id="sw-gradient-0" x1="0" x2="0" y1="1" y2="0">
					<stop stopColor="rgba(38, 38, 38, 1)" offset="0%"></stop>
					<stop stopColor="rgba(84.561, 84.561, 84.561, 1)" offset="100%"></stop>
				</linearGradient>
			</defs>
			<path
				fill="url(#sw-gradient-0)"
				d="M0,10L480,50L960,20L1440,80L1920,10L2400,70L2880,90L3360,40L3840,60L4320,0L4800,50L5280,90L5760,20L6240,0L6720,70L7200,10L7680,70L8160,0L8640,50L9120,80L9600,0L10080,70L10560,0L11040,90L11520,60L11520,100L11040,100L10560,100L10080,100L9600,100L9120,100L8640,100L8160,100L7680,100L7200,100L6720,100L6240,100L5760,100L5280,100L4800,100L4320,100L3840,100L3360,100L2880,100L2400,100L1920,100L1440,100L960,100L480,100L0,100Z"
			></path>
		</svg>
	);
};
