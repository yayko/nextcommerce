"use server";

import { cookies } from "next/headers";
import { revalidateTag } from "next/cache";
import { executeGraphQl } from "@/api/baseGraphQLRequest";
import {
	CartCreateDocument,
	CartGetCartItemsListDocument,
	type CartGetCartItemsListQuery,
	CartItemAddDocument,
	CartItemRemoveDocument,
	CartItemUpdateQuantityDocument,
	GetCartTotalsDocument,
	ProductGetByIdDocument,
} from "@/gql/graphql";

export async function addToCartAction(formData: FormData) {
	const { cart } = await getOrCreateCart();
	const productId = formData.get("productId");

	if (!productId) {
		throw new Error("[FE] Product not found");
	}

	if (!cart) {
		throw new Error("[FE] Prodcut not found");
	}

	await addProductToCart(cart.id, productId as string);
	revalidateTag("cart");
}

export async function updateCartItemQuantityAction(id: string, quantity: number) {
	return executeGraphQl({
		query: CartItemUpdateQuantityDocument,
		variables: {
			id,
			quantity,
		},
	});
}

export async function removeCartItemAction(cartItemId: string) {
	await executeGraphQl({
		query: CartItemRemoveDocument,
		variables: {
			id: cartItemId,
		},
	});
	revalidateTag("cart");
}

export async function getOrCreateCart() {
	const cartId = cookies().get("cartId")?.value;
	if (cartId) {
		const { cart, cartTotals } = await executeGraphQl({
			query: CartGetCartItemsListDocument,
			variables: {
				cartId,
			},
		});
		if (cart) {
			return {
				cart,
				cartTotals,
			} as CartGetCartItemsListQuery;
		}
	}

	const { cartCreate: newCart } = await executeGraphQl({ query: CartCreateDocument });
	if (!newCart) {
		throw new Error("Failed to create cart");
	}

	const { cartTotals } = await executeGraphQl({
		query: GetCartTotalsDocument,
		variables: { id: newCart.id },
	});

	cookies().set("cartId", newCart.id);
	return {
		cart: newCart,
		cartTotals,
	};
}

/****
 
 			Helper functions

 ****/
async function addProductToCart(cartId: string, productId: string) {
	const { product } = await executeGraphQl({
		query: ProductGetByIdDocument,
		variables: {
			id: productId,
		},
	});
	if (!product) {
		throw new Error(`Product with id ${productId} not found`);
	}
	await executeGraphQl({
		query: CartItemAddDocument,
		variables: {
			cartId,
			productId,
		},
	});
}
