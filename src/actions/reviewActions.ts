"use server";

import { revalidateTag } from "next/cache";
import { executeGraphQl } from "@/api/baseGraphQLRequest";
import { RevievCreateDocument, type ReviewAddDto } from "@/gql/graphql";

export const addReview = async (formData: FormData) => {
	await executeGraphQl({
		query: RevievCreateDocument,
		variables: {
			data: {
				title: formData.get("headline"),
				content: formData.get("content"),
				email: formData.get("email"),
				name: formData.get("name"),
				rating: parseInt(formData.get("rating") as string),
				productId: formData.get("productId"),
			} as unknown as ReviewAddDto,
		},
	});

	revalidateTag("reviews");
};
